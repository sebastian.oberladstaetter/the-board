<?php
// src/Controller/MorfController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MorfController extends AbstractController
{
    public function number()
    {
        $mytext = $_POST['morfpercent'];
        $filename = '../morf.txt';
        file_put_contents($filename, $mytext);
        return $this->redirectToRoute('app_project', [], 301);
    }
}



