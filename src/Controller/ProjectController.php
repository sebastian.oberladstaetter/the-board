<?php



namespace App\Controller;
// set_time_limit(0);
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ProjectController extends AbstractController
{
    public $client;

    public function __construct() {
        $RedApiKey = 'd9ececc41f7c75dbd7fde8ae8c93275f454121fc';
        $this->client = new \Redmine\Client('https://customerportal.holzweg.tv/', $RedApiKey);
        $this->client->setCheckSslCertificate(true);
    }

    public function tickets()
    {        
        $arrUser = [];
        $projectsToExclude = ['MORF AG' => 463, 'Intern' => 414, 'IKB Lyfecyclemanagement' => 534, 'EGLO Stundenkontingent' => 523, 'Holzweg.com' => 415, 'Allgemeine Arbeiten' => 510, 'Bio Heizwerk Steinach' => 543, 'Musikschule' => 546, 'AK-Quiz Tool' => 505, 'Kundenportal Leitwind' => 393, 'interne Tools' => 547]; //name => id ; name has to match
        
        //fetch open Tickets
        $issuesOpen = $this->client->issue->all([
            'status_id' => 'open',
            'limit' => 200
        ]);

        //get all IDs
        $projectOpenId = [];

        //get all names
        $projectOpenName = [];

        foreach ($issuesOpen['issues'] as $issue) {
            $projectOpenedId = $issue['project']['id'];
            $projectOpenedName = $issue['project']['name'];
            array_push($projectOpenId, $projectOpenedId);
            array_push($projectOpenName, $projectOpenedName);
        }
        
        //remove redundant IDs
        $projectsOpenId = array_unique($projectOpenId);

        //remove redundant names
        $projectsOpenName = array_unique($projectOpenName);

        //put names as keys and IDs as values
        $projectsOpen = array_combine($projectsOpenName, $projectsOpenId);

        //sort by names alphabetical
        ksort($projectsOpen);

        // Morf is last | none-important projects are excluded
        $projectsOpenFinal = array_diff($projectsOpen, $projectsToExclude);
        $projectsOpenFinal += ['Intern' => 414, 'MORF AG' => 463];

        //execute function
        list($ticketsFromAllPortals, $arrUser) = $this->getTicketsFromProject($projectsOpenFinal, $issuesOpen);


        $filename = '../morf.txt';
        $morfPercent = file_get_contents($filename);
        

        return $this->render('project/details.html.twig', [
            'allTickets' => $ticketsFromAllPortals,
            'users' => $arrUser,
            'morf' => $morfPercent
        ]);      
    }
        
    // get ProjectTickets with the right order
    public function getTicketsFromProject($projectsOpenFinal, $issuesOpen)
    {
        // 'status_id' => x, // 1 - I_A 2 - QK_intern 6 - offen 7 - Archiv 8 - Gesichtet 9 - B_z_A 11 - K_RM 12 - später
        $usersToExclude = [ 10321, 10336,  10374, 10322, 10206, 10365, 10239, 10350, 3305, 10139, 10292, 101, 10152]; 
        // Sandra, Service, Mario, Daniel Pauser, Testkunde, Test-dev, hwadmin, Roboter, Gabi Kuehn, Florian Peter, customer, Christoph Holz, Barbara Thaler
        // 10323 - Feras 10319 - Lukas 10285 - Simon 10148 - Georg 10380 - Larissa 10321 - Sandra, 10372 - Emirhan, 10386 - Sebastian
        $relStatus = array(1, 6, 8);

        $arrUserId = [];
        $arrUsersFull = [];
        
        //get Users
        $idInt = [];
        for($i=10300; $i<10400; $i++){
            
                array_push($idInt,10148,10285, $i);
            
        }
        $userArr = $this->client->user->all(
            ['limit' => 1000]
        );       


        $hwUsers = [];

        foreach ($userArr['users'] as $userInfo){
            $arrUsers = [];
            $mail = '';
            $id = $userInfo['id'];
            if(array_key_exists('mail', $userInfo)){
                $mail = $userInfo['mail'];
            }
            
            if(strpos($mail, 'holzweg') && !in_array($id, $usersToExclude)){
                array_push($hwUsers, $userInfo);
                array_push($arrUserId, $userInfo['id']);
                array_push($arrUsers, $userInfo['firstname'][0], $userInfo['id'], $userInfo['firstname'], $userInfo['lastname']);
                $arrUsers[0] .= $userInfo['lastname'][0];
                array_push($arrUsersFull, $arrUsers);
            }
        } 

        //get Tickets
        foreach ($projectsOpenFinal as $key => $value) {
            $varHold = [];
            $varQK = [];
            $varK_RM = [];
            $varDone = [];
            $usersZwl = [];        
            $otherUsers = ['tasks' =>[]];

            foreach($arrUserId as $user) {
                $usersZwl[$user] = [];
            }

            foreach ($issuesOpen['issues'] as $ticket){
                $project = $ticket['project']['id'];
                $status = ['id' => $ticket['status']['id'], 'name' => $ticket['status']['name']];
                $card = ['id' => $ticket['id'], 'subject' => $ticket['subject']];
                if (array_key_exists('assigned_to', $ticket)){
                    $card['user']['name'] = $ticket['assigned_to']['name'];
                    $card['user']['id'] = $ticket['assigned_to']['id'];
                }

                if($project == $value){
                    if(array_key_exists('assigned_to', $ticket)){
                        $assigned = $ticket['assigned_to']['id'];
                    }else{
                        $assigned = $ticket['author']['id'];
                    }
                    if( $status['id'] == 12){
                        array_push($varHold, $card);                   
                    }else if($status['id'] == 11){
                        array_push($varK_RM, $card);  
                    }else if($status['id'] == 9) {
                        array_push($varDone, $card);                       
                    }else if($status['id'] == 2) {
                        array_push($varQK, $card);                     
                    }else if(in_array($status['id'], $relStatus)){
                        foreach ($arrUserId as $user) {
                            if(in_array($assigned, $arrUserId)){
                            
                                if ($user == $assigned) {
                                    if (array_key_exists($user, $usersZwl)) {
                                        array_push($usersZwl[$user], $card);
                                    }else {
                                        $usersZwl[$user] = [ 0 => $card];
                                    }
                                }
                            }else {
                                if (array_key_exists('tasks', $otherUsers)) {
                                    if (!in_array($card, $otherUsers['tasks']))
                                    array_push($otherUsers['tasks'], $card);
                                }else {
                                    $otherUsers['tasks'] = [ 0 => $card];
                                }
                            }
                        }       
                    }    
                }
            }

            $varOthers = [$varHold, $varQK, $varK_RM, $varDone];

            $varOutput = ['workers' => $usersZwl, 'customers' => $otherUsers, 'others' => $varOthers];
            $ticketsFromAllPortals[$key] = $varOutput;
            
        }
        // dump($ticketsFromAllPortals);
        // dump($varAllUsers);
        return array($ticketsFromAllPortals, $arrUsersFull);
    }
}    
   