<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{

    public function index()
    {
        $users = ['Larissa' , 'Sebastian', 'Georg', 'Lukas', 'Simon', 'Sandra', 'Emirhan'];
      


        return $this->render('home/index.html.twig', [
         
            'users' => $users,
            
        ]);      
    }
}