<?php

namespace Hw\HwProvider\Controller;

use \TYPO3\CMS\Extbase\Mvc\Controller\ActionController as ActionController;
use Hw\HwProvider\Helpers\ContentHelperTrait as ContentHelper;
use PhpParser\Node\Expr\Cast\Array_;
use PhpParser\Node\Stmt\Return_;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class TodoController extends ActionController
{
    use ContentHelper;

    public function getTodoAction()
    {
        //api key from sebi user
        $redmineApiKey = '51bb5a018a84ca406854839760127298cce2726c';

        
        $client = new \Redmine\Client('https://customerportal.holzweg.tv/', $redmineApiKey);


        $client->setCheckSslCertificate(true);

        //get user api key
        $username = $GLOBALS['TSFE']->fe_user->user['username'];
        // if (empty($username)) return json_encode('USER_NOT_LOGGED_IN');

        $userid = $client->user->getIdByUsername($username, ['limit' => 1000]);
        // if ($userid === false) return json_encode('REDMINE_USER_NOT_FOUND');

        $user_groups = $client->user->show($userid)['user']['groups'];
        // $user_apikey = $client->user->show($userid)['user']['api_key'];
        $allTickets = [];
        $allTicketsDeclined = [];

        // Fetch group tickets
        foreach ($user_groups as $user_group) {
            $groupId = $user_group['id'];

            //redmine group "angestellte" = 9
            //if ($groupId === 9) continue;

            $tickets = $client->issue->all([
                'limit' => 50,
                'assigned_to_id' => $groupId
            ]);
            $ticketsDeclined = $client->issue->all([
                'limit' => 50,
                'status_id' => 4,
                'author_id' => $groupId
            ]);

            array_push($allTickets, $tickets);
            array_push($allTicketsDeclined, $ticketsDeclined);
        }

        // Fetch user tickets
        $tickets = $client->issue->all([
            'limit' => 50,
            'assigned_to_id' => $userid
        ]);
        $ticketsDeclined = $client->issue->all([
            'limit' => 50,
            'status_id' => 4,
            'author_id' => $userid
        ]);

        array_push($allTickets, $tickets);
        array_push($allTicketsDeclined, $ticketsDeclined);

        return json_encode(['ticketsDeclined' => $allTicketsDeclined, 'tickets' => $allTickets, 'apikey' => $user_apikey]);
    }
}










// $laOpen= array('1', '6', '8');
        // $ticketsLAnew = [];

        // $ticketsLA = $client->issue->all([
        //     'limit' =>$limit,
        //     'sort' => $sorter,
        //     'assigned_to_id' => $userToId['LA'],
        //     'status_id' => $laOpen
        // ]);

        // foreach ($ticketsLA as $test) {
        //     $ticketsLAnew1=$client->issue->all([
        //         'status_id' =>$stateToId['IA']
        //     ]);
        //     $ticketsLAnew2=$client->issue->all([
        //         'status_id' =>$stateToId['open']
        //     ]);
        //     $ticketsLAnew3=$client->issue->all([
        //         'status_id' =>$stateToId['seen']
        //     ]);
        //     array_push($ticketsLAnew, [$ticketsLAnew1, $ticketsLAnew2, $ticketsLAnew3]);
        // }

        $ticketsLA1 = $client->issue->all([
            'limit' =>$limit,
            'status_id' => $stateToId['IA'],
            'sort' => $sorter,
            'assigned_to_id' => $userToId['LA']

        ]);
        $ticketsLA6 = $client->issue->all([
            'limit' =>$limit,
            'status_id' => $stateToId['open'],
            'sort' => $sorter,
            'assigned_to_id' => $userToId['LA']

        ]);
        $ticketsLA8 = $client->issue->all([
            'limit' =>$limit,
            'status_id' => $stateToId['seen'],
            'sort' => $sorter,
            'assigned_to_id' => $userToId['LA']

        ]);



        $ticketsSM1 = $client->issue->all([
            'limit' =>$limit,
            'status_id' => $stateToId['IA'],
            'sort' => $sorter,
            'assigned_to_id' => $userToId['SM']

        ]);
        $ticketsSM6 = $client->issue->all([
            'limit' =>$limit,
            'status_id' => $stateToId['open'],
            'sort' => $sorter,
            'assigned_to_id' => $userToId['SM']

        ]);
        $ticketsSM8 = $client->issue->all([
            'limit' =>$limit,
            'status_id' => $stateToId['seen'],
            'sort' => $sorter,
            'assigned_to_id' => $userToId['SM']

        ]);

        $ticketsGB1 = $client->issue->all([
            'limit' =>$limit,
            'status_id' => $stateToId['IA'], 
            'sort' => $sorter,
            'assigned_to_id' => $userToId['GB']

        ]);
        $ticketsGB6 = $client->issue->all([
            'limit' =>$limit, 
            'status_id' => $stateToId['open'],
            'sort' => $sorter,
            'assigned_to_id' => $userToId['GB']

        ]);$ticketsGB8 = $client->issue->all([
            'limit' =>$limit,
            'status_id' => $stateToId['seen'],
            'sort' => $sorter,
            'assigned_to_id' => $userToId['GB']

        ]);

        $ticketsLH1 = $client->issue->all([
            'limit' =>$limit,
            'status_id' => $stateToId['IA'],
            'sort' => $sorter,
            'assigned_to_id' => $userToId['LH']

        ]);
        $ticketsLH6 = $client->issue->all([
            'limit' =>$limit,
            'status_id' => $stateToId['open'],
            'sort' => $sorter,
            'assigned_to_id' => $userToId['LH']

        ]); 
        $ticketsLH8 = $client->issue->all([
            'limit' =>$limit,
            'status_id' => $stateToId['seen'],
            'sort' => $sorter,
            'assigned_to_id' => $userToId['LH']

        ]);

       


        

        

        
        //dump($ticketsLA);
        //dump($ticketsUser);
        //dump($ticketsHold);


        'ticketsLA1' => $ticketsLA1['issues'],
        'ticketsLA6' => $ticketsLA6['issues'],
        'ticketsLA8' => $ticketsLA8['issues'],
        'ticketsSM1' => $ticketsSM1['issues'],
        'ticketsSM6' => $ticketsSM6['issues'],
        'ticketsSM8' => $ticketsSM8['issues'],
        'ticketsGB1' => $ticketsGB1['issues'],
        'ticketsGB6' => $ticketsGB6['issues'],
        'ticketsGB8' => $ticketsGB8['issues'],
        'ticketsLH1' => $ticketsLH1['issues'],
        'ticketsLH6' => $ticketsLH6['issues'],
        'ticketsLH8' => $ticketsLH8['issues'],


        $users = $client->user->all([
            'limit' => 200,
            
            
        ]); 
           
            
        //dump($users);

        'users' => $users['users'],


            // 'ticketsLA' => $ticketsLA['issues'],
            // 'ticketsLAnew' => $ticketsLAnew['issues'],



            //'ticketCount' => $tickets['total_count'],





   // alle Tickets auf Hold
        // $ticketsOnHold = [];


             // foreach ($a as $onHold) {
        //     array_push($ticketsOnHold, 'a');
        // }


        //test
        //$a =['a', 'b', 'c' , 'd', 'e' , 'f'];


        $ticketsGebro = $client->issue->all([
            'limit' =>$limit,
            'project_id' => 395
        ]);
        //dump($ticketsGebro);





         //for für User

         $ticketsInUse =[];

         for ($i=0; $i < 4; $i++) { 
             $ticketsUsers = $client->issue->all([
                 'limit' => $limit,
                 'sort' => $sorter,
                 'assigned_to_id' => $usersInUse[$i]
             ]);
             array_push($ticketsInUse, $ticketsUsers);
             //dump($ticketsUsers);
         }
         
         //dump($ticketsInUse);
         'ticketsInUse' => $ticketsInUse,





            

        $ticketsHold = $client->issue->all([
            'limit' =>$limit,
            'status_id' => $stateToId['hold'],
            'sort' => $sorter,
            'assigned_to_id' => '*'

        ]);



$ticketsQK = $client->issue->all([
    'limit' =>$limit,
    'status_id' => $stateToId['QK'],
    'sort' => $sorter,
    'assigned_to_id' => '*'

]);

$ticketsDone = $client->issue->all([
    'limit' =>$limit,
    'status_id' => $stateToId['BzA'],
    'sort' => $sorter,
    'assigned_to_id' => '*'

]);

$ticketsK_RM = $client->issue->all([
    'limit' =>$limit,
    'status_id' => $stateToId['K_RM'],
    'sort' => $sorter,
    'assigned_to_id' => '*'

]);


'ticketsHold' => $ticketsHold['issues'],
'ticketsDone' => $ticketsDone['issues'],
'ticketsQK' => $ticketsQK['issues'], 
'ticketsK_RM' => $ticketsK_RM['issues'],






<tr>
<th>Projekt</th>
<th>Lukas</th>


</tr>

{% for user in ticketsUser %}
{% for ticket in user.issues %}
<tr>
<td>{{ticket.project.name}} {{ticket.project.id}}</td>
<td>{{ticket.id}} {{ticket.subject}}</td>
 <td>{{ticket.status.name}}</td>

</tr>
 {% endfor %}
{% endfor %}

{% for i in tickets %}
<tr>
<td>{{i.project.name}}</td>
<td>{{i.project.id}}</td>
 <td>{{i.status.name}}</td>

</tr>
{% endfor %}
{% for i in ticketsLH8 %}
<tr>
<td>{{i.project.name}}</td>
<td>{{i.id}} {{i.subject}}</td>
 <td>{{i.status.name}}</td>

</tr>
{% endfor %}

<tr>
<th>Projekt</th>
<th>Simon</th>


</tr>

{% for i in ticketsSM1 %}
<tr>
<td>{{i.project.name}}</td>
<td>{{i.id}} {{i.subject}}</td>
 <td>{{i.status.name}}</td>

</tr>
{% endfor %}
{% for i in ticketsSM6 %}
<tr>
<td>{{i.project.name}}</td>
<td>{{i.id}} {{i.subject}}</td>
 <td>{{i.status.name}}</td>

</tr>
{% endfor %}
{% for i in ticketsSM8 %}
<tr>
<td>{{i.project.name}}</td>
<td>{{i.id}} {{i.subject}}</td>
 <td>{{i.status.name}}</td>

</tr>
{% endfor %}

<tr>
<th>Projekt</th>
<th>Georg</th>
<th>Status</th>


</tr>

{% for i in ticketsGB1 %}
<tr>
<td>{{i.project.name}}</td>
<td>{{i.id}} {{i.subject}}</td>
<td>{{i.status.name}}</td>

</tr>
{% endfor %}
{% for i in ticketsGB6 %}
<tr>
<td>{{i.project.name}}</td>
<td>{{i.id}} {{i.subject}}</td>
<td>{{i.status.name}}</td>

</tr>
{% endfor %}
{% for i in ticketsGB8 %}
<tr>
<td>{{i.project.name}}</td>
<td>{{i.id}} {{i.subject}}</td>
<td>{{i.status.name}}</td>

</tr>
{% endfor %}

<tr>
<th>Projekt</th>
<th>Larissa</th>


</tr>

{# {% for i in ticketsLA %}
<tr>
<td>{{i.project.name}}</td>
<td>{{i.id}} {{i.subject}}</td>
 <td>{{i.status.name}}</td>

</tr>
{% endfor %} #}

{% for i in ticketsLA1 %}
<tr>
<td>{{i.project.name}}</td>
<td>{{i.id}} {{i.subject}}</td>
 <td>{{i.status.name}}</td>

</tr>
{% endfor %}
{% for i in ticketsLA6 %}
<tr>
<td>{{i.project.name}}</td>
<td>{{i.id}} {{i.subject}}</td>
 <td>{{i.status.name}}</td>

</tr>
{% endfor %}
{% for i in ticketsLA8 %}
<tr>
<td>{{i.project.name}}</td>
<td>{{i.id}} {{i.subject}}</td>
 <td>{{i.status.name}}</td>

</tr>
{% endfor %}

<tr>
<th>Projekt</th>
<th>Hold</th>


</tr>

{% for i in ticketsHold %}
<tr>
<td>{{i.project.name}}</td>
<td>{{i.id}} {{i.subject}}</td>

</tr>
{% endfor %}


<tr>
<th>Projekt</th>
<th>QK</th>


</tr>



{% for i in ticketsQK %}
<tr>
<td>{{i.project.name}}</td>
<td>{{i.id}} {{i.subject}}</td>

</tr>
{% endfor %}

<tr>
<th>Projekt</th>
<th>Kundenrückmeldung</th>


</tr>



{% for i in ticketsK_RM %}
<tr>
<td>{{i.project.name}}</td>
<td>{{i.id}} {{i.subject}}</td>

</tr>
{% endfor %}

<tr>
<th>Projekt</th>
<th>Done</th>


</tr>



{% for i in ticketsDone %}
<tr>
<td>{{i.project.name}}</td>
<td>{{i.id}} {{i.subject}}</td>

</tr>
{% endfor %}


 //alle Tickets

 $tickets = $client->issue->all([
    'limit' => 200,
    'sort' => $sorter,
    'assigned_to_id' => '*',


]);

//alle Tickets je Project

$ticketsProject = [];
    
foreach ($projectToId as $project) {

//alle Tickets je User

$ticketsUser=[];


foreach ($userToId as $userId) {


    $ticketsUserzwl = $client->issue->all([
        'limit' => $limit,
        'assigned_to_id' => $userId,
        'sort' => $sorter,

    ]);
array_push($ticketsUser, $ticketsUserzwl);

    $ticketsProjects = $client->issue->all([
        'limit' => $limit,
        'project_id' => $project,
        'assigned_to_id' => '*',
        'sort' => $sorter
       
    ]); 
}


array_push($ticketsProject, $ticketsProjects);
    
}


$result = array_merge($ticketsProject, $ticketsUser);

// dump($ticketsProject);

// get UserTickets for relevant Projects and Status (in Arbeit, offen, gesichtet)

public function getTicketsForUser($user, $client)
{
   $newArray= [];
   $relProjects = array( 411, 442, 411, 446, 537, 466, 449, 463);
   $relStatus = array(1, 6, 8);

   for ($i=0; $i < count($relProjects); $i++) { 


    foreach ($relStatus as $status) {
        $var = $client->issue->all([
            'project_id' => $relProjects[$i],
            'assigned_to_id' => $user,
            'status_id' => $status
        ]);
        
        array_push($newArray, $var);
        
    }
}

   return array($newArray);
}





// get StatusTickets for relevant Projects and Users (Lukas, Georg, Simon, Larissa)

public function getTicketsForStatus($status, $client)
{

    $output= [];
    $relProjects = array( 411, 442, 411, 446, 537, 466, 449, 463);
    $usersInUse = array(10319, 10285, 10148, 10380);

    for ($i=0; $i < count($relProjects); $i++) { 


        foreach ($usersInUse as $users) {
            $var = $client->issue->all([
                'project_id' => $relProjects[$i],
                'assigned_to_id' => $users,
                'status_id' => $status
            ]);
            array_push($output, $var);
        }
    }

    return array($output);

}


'ticketsLA' => $ticketsLA,
'ticketsSM' => $ticketsSM,
'ticketsGB' => $ticketsGB,
'ticketsLH' => $ticketsLH,

'ticketsHold' => $ticketsHold,
'ticketsDone' => $ticketsDone,
'ticketsK_RM' => $ticketsK_RM,
'ticketsQK_I' => $ticketsQK_I,



'projectArr' => $projectArr,
'projectToId' => $projectToId,



        //array for correct order
        $orderarrstatus = array(2, 11, 9);
        $orderarruser = array(10319, 10285, 10148, 10380);





        // Tickets for User (status 1, 6 und 8)
        $ticketsLA = $this->getTicketsForUser($userToId['LA'], $client);
        $ticketsSM = $this->getTicketsForUser($userToId['SM'], $client);
        $ticketsGB = $this->getTicketsForUser($userToId['GB'], $client);
        $ticketsLH = $this->getTicketsForUser($userToId['LH'], $client);



        // Tickets for Status
        $ticketsHold = $this->getTicketsForStatus($stateToId['hold'], $client);
        $ticketsDone = $this->getTicketsForStatus($stateToId['BzA'], $client);
        $ticketsK_RM = $this->getTicketsForStatus($stateToId['K_RM'], $client);
        $ticketsQK_I = $this->getTicketsForStatus($stateToId['QK'], $client);

        $projectArr =array('DIBK', '','','', 'EGLO','','','', 'GEBRO','','','', 'IKB','','','', 'NHT','','','', 'SAHALL','','','', 'WEDL','','','', 'MORF','','','',);


          
        $opentasks = array(1, 6, 8);



        // users
        // 10319 - Lukas 10285 - Simon 10148 - Georg 10380 - Larissa 10321 - Sandra, 10372 - Emirhan, 10386 - Sebastian

        $idToUser = array(10319=>'LH', 10285=>'SM', 10148=>'GB', 10380=>'LA'); 
        $userToId = array('LH'=>10319, 'SM'=>10285, 'GB'=>10148, 'LA'=>10380, 'SO' => 10386, 'EG' => 10372);
        $usersInUse = array(10319, 10285, 10148, 10380);
        $limit = 5;
        $sorter = 'assigned_to';


   // 'status_id' => x, // 1 - I_A 2 - QK_intern 6 - offen 7 - Archiv 8 - Gesichtet 9 - B_z_A 11 - K_RM 12 - später

   $stateToId = array('IA' => 1, 'QK' => 2, 'open' => 6, 'archiv' => 7, 'seen' => 8, 'BzA' => 9, 'K_RM' => 11, 'hold' => 12);
     

   <script>var search_site='DOMAIN.TLD'; </script>
<form name=metasearch method=POST accept-charset="UTF-8" action="https://www.startpage.com/do/search" style=" margin:0; padding:0; " onSubmit="javascript:document.metasearch.query.value='site:' + search_site + ' ' + document.metasearch.keyword.value; "><table border="0 " cellspacing="0 " cellpadding="3 " bgcolor="#ffffff " style=" border:0px solid gray "><tr><td><a href="https://onsite.org" target="_blank"> <img src="https://www.onsite.org/assets/images/onsite_surfer_200.png" style="height: 30px; width: auto"></a></td><td><input type=hidden name=from value=searchbox><input name=language type=hidden value=deutsch><input type=hidden name=cat value=web><input name=query type=hidden size=32><input name=keyword type=text size=32><input type=hidden name=cmd value="process_search "><input type=hidden name=frm value=sb> </td><td><input type=Submit value="Suche "></td></tr></table><!-- Serach provided by onsite.org --></form>